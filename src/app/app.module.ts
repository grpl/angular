import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { appRoutingModule } from "./app.routing";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { SearchComponent } from "./components/search";
import { DetailsComponent } from "./components/details";

@NgModule({
  declarations: [AppComponent, SearchComponent, DetailsComponent],
  imports: [
    BrowserModule,
    appRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
