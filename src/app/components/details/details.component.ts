import { Component } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { find } from "rxjs/operators";
import { Character } from "../../models/character.interface";
import { SearchService } from "../../services/search.service";
import { Observable } from "rxjs/Observable";

@Component({
  templateUrl: "details.component.html"
})
export class DetailsComponent {
  characterObservable$: Observable<Character>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private searchService: SearchService
  ) {
    //In this case, according to the documentation, we do not have to stop subscribing to the observer.
    //https://angular.io/guide/router#!#route-parameters
    this.route.params
      .pipe(find(obj => obj.hasOwnProperty("id")))
      .subscribe(params => {
        //In the future, we can read the character object from the list.
        //Now, after refreshing or forwarding the link, the page will load data.
        this.characterObservable$ = this.searchService.getCharacterById(
          params.id
        );
      });
  }
}
