import { Component, OnInit } from "@angular/core";
import { SearchService } from "../../services/search.service";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/combineLatest";
import { distinctUntilChanged, tap, map } from "rxjs/operators";
import { Character } from "../../models/character.interface";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";

@Component({
  templateUrl: "search.component.html",
  providers: [SearchService]
})
export class SearchComponent implements OnInit {
  charactersObservable$: Observable<Character[]>;
  nameControl = new FormControl("");
  selectControl = new FormControl("");
  formGroup: FormGroup = this.fb.group({
    name: [""],
    gender: [""]
  });
  currentPage = 1;
  nextPageAvailable = false;
  loading = true;

  constructor(private searchService: SearchService, private fb: FormBuilder) {
    const nameValues$ = this.formGroup.get("name").valueChanges;
    const genderValues$ = this.formGroup.get("gender").valueChanges;

    nameValues$.pipe(distinctUntilChanged()).subscribe(name => {
      this.resetCurrentPage();
      this.loading = true;
      this.searchService.setParam("name", name).sendRequest();
    });

    genderValues$.pipe(distinctUntilChanged()).subscribe(gender => {
      this.resetCurrentPage();
      this.loading = true;
      this.searchService.setParam("gender", gender).sendRequest();
    });
  }

  ngOnInit() {
    this.charactersObservable$ = this.searchService
      .sendRequest()
      .resoults$.pipe(
        tap(response => (this.nextPageAvailable = !!response.info.next)),
        map(response => response.results as Character[]),
        tap(() => (this.loading = false))
      );
  }

  clearForm() {
    this.formGroup.reset({
      name: "",
      gender: ""
    });
    this.resetCurrentPage();
    this.searchService.sendRequest();
  }

  private resetCurrentPage() {
    this.currentPage = 1;
    this.searchService.setParam("page", this.currentPage.toString());
  }

  nextPage() {
    if (!this.nextPageAvailable) {
      return;
    }

    this.currentPage++;
    this.loadPage();
  }

  prevPage() {
    if (this.currentPage === 1) {
      return;
    }

    this.currentPage--;
    this.loadPage();
  }

  private loadPage() {
    this.loading = true;
    this.searchService
      .setParam("page", this.currentPage.toString())
      .sendRequest();
  }
}
