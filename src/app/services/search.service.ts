import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import "rxjs/add/operator/map";
import { CharacterResponse, Character } from "../models/character.interface";
import { Subject } from "rxjs";
import {
  distinctUntilChanged,
  debounceTime,
  switchMap,
  catchError
} from "rxjs/operators";
import { of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class SearchService {
  resoults$ = new Subject<CharacterResponse>();

  private queryParams: {
    [key: string]: string;
  } = {};
  private search$ = new Subject<string>();
  private baseUrl: string = "https://rickandmortyapi.com/api/character";

  constructor(private httpClient: HttpClient) {
    this.search$
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        switchMap(url =>
          this.httpClient.get<CharacterResponse>(url).pipe(
            catchError(() => {
              return of({
                info: {},
                results: []
              } as CharacterResponse);
            })
          )
        )
      )
      .subscribe(response => {
        this.resoults$.next(response);
      });
  }

  setParam(key: string, value: string) {
    this.queryParams[key] = value;

    return this;
  }

  resetParams() {
    this.queryParams = {};

    return this;
  }

  private getFullUrl(): string {
    let params: string = new HttpParams({
      fromObject: this.queryParams
    }).toString();

    return `${this.baseUrl}/?${params}`;
  }

  sendRequest() {
    this.search$.next(this.getFullUrl());

    return this;
  }

  getCharacterById(id: number) {
    return this.httpClient.get<Character>(`${this.baseUrl}/${id}`);
  }
}
