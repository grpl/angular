export interface Character {
  "id": number,
  "name": string,
  "species": string,
  "gender": string,
}

interface Info {
  "count": number
  "pages": number
  "next": string
  "prev": string
}

export interface CharacterResponse {
  info: Info
  results: Character[]
}