import { Routes, RouterModule } from "@angular/router";

import { SearchComponent } from "./components/search";
import { DetailsComponent } from "./components/details";

const routes: Routes = [
  { path: "", component: SearchComponent },
  { path: "details/:id", component: DetailsComponent },

  // otherwise redirect to home
  { path: "**", redirectTo: "" }
];

export const appRoutingModule = RouterModule.forRoot(routes);
